from django.conf.urls import url
from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import *

# from django.views.static.
urlpatterns = [
    path('', listaCursos.as_view() , name='p_inicio'), 
    path('detalle/<slug:slug_text>/', login_required( detalleCurso.as_view()), name='detalle')   
]

app_name="app_cursos"
  