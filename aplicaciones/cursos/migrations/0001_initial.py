# Generated by Django 2.2.2 on 2020-05-13 19:23

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=150)),
                ('slug', models.SlugField(editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='Curso',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
                ('slug', models.SlugField(editable=False, max_length=150, unique=True)),
                ('resumen', models.TextField(max_length=250, null=True)),
                ('sobre_curso', models.TextField(max_length=2000)),
                ('portada', models.ImageField(null=True, upload_to='baners_cursos')),
                ('duracion', models.IntegerField()),
                ('fecha_creacion', models.DateTimeField(auto_now=True)),
                ('estado', models.BooleanField(default=True)),
                ('es_gratis', models.BooleanField(default=False)),
                ('costo', models.DecimalField(decimal_places=2, default=0.0, max_digits=5)),
                ('descuento', models.DecimalField(decimal_places=2, default=0.0, max_digits=5)),
                ('autores', models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL)),
                ('categorias', models.ManyToManyField(to='cursos.Categoria')),
            ],
        ),
    ]
