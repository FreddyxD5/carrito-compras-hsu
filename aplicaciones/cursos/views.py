from django.shortcuts import render
from django.views.generic import View,TemplateView,DetailView,ListView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Curso as mis_cursos
# Create your views here.

class Curso(TemplateView):
    template_name = 'cursos/cursos.html'

class Index_principal(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Index_principal,
                        self).get_context_data(**kwargs)
        context['cursos'] = mis_cursos.objects.filter(estado=True)
        return context


class listaCursos(View):

    template_name = 'cursos/cursos.html'

    def get_context_data(self, **kwargs):
        context = super(listaCursos,
                        self).get_context_data(**kwargs)
        context['cursos'] = mis_cursos.objects.filter(estado=True)
        return context


#Vistas basadas en Clases

class listaCursos(ListView):
    model = mis_cursos
    template_name = "cursos/cursos.html"
    context_object_name="lista_cursos"


class detalleCurso(DetailView):
    model = mis_cursos
    template_name="cursos/curso_detalle.html"

    def get_queryset(self,slug_text,**kwargs):        
    	return mis_cursos.objects.get(slug = slug_text, estado = True)

    def get_context_data(self,slug_text,**kwargs):
	    context = {}
	    context['curso_detalle'] = self.get_queryset(slug_text)
	    return context

    def get(self,request,slug_text,*args,**kwargs):
	    return render(request,self.template_name,self.get_context_data(slug_text))
    


#Vistas basadas en funciones
@login_required
def lista_cursos(request):
    lista_cursos =  mis_cursos.objects.all()
    context = {'lista_cursos':lista_cursos}
    return render(request,'cursos/cursos.html', context)

@login_required
def detalle_curso(request, slug_text):
    curso_detalle = mis_cursos.objects.get(slug = slug_text)
    print(curso_detalle)
    context = {'curso_detalle':curso_detalle}
    return render(request, 'cursos/curso_detalle.html', context)