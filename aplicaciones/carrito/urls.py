from django.conf.urls import url
from django.urls import re_path
from .views import *
# from django.views.static.
urlpatterns = [
    re_path(r'^agregar_carrito_compra/(?P<slug>[^/]+)/$', agregar_curso_carrito, name='p_agregar_compra'),
    re_path(r'^ver_carrito/(?P<usuario>[^/]+)$', ver_carrito, name='p_ver_carrito'),
]
app_name="app_carrito"