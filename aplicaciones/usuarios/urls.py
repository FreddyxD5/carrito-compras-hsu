from django.conf.urls import url
from django.urls import path,re_path
# para poder importar las imagenes
from django.views.static import serve
from .views import *
# from views import Usuarios
from django.conf import settings

# from django.views.static.
urlpatterns = [
    # url(r'^usuarios', Usuarios.as_view(), name='inicio'),
    # url(r'^$', UsuarioVer.as_view(), name='p_inicio'),
    path('', UsuarioVer.as_view(), name='p_inicio'),
    re_path(r'^media/(?P<path>.*)$', serve, {
        'document_root': settings.MEDIA_ROOT,
    }),
]
app_name="app_usuario"
 